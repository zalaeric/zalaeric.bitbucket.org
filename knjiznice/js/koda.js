
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

function vstopProf() {
	$("#profAliNavadniContainer").attr("style", "display: none");
	$("#ena").attr("style", "");
	$("#dva").attr("style", "");
	$("#tri").attr("style", "");
	$("#generirajJedilnikContainer").attr("style", "");
}

function vstopNavaden() {
	$("#profAliNavadniContainer").attr("style", "display: none");
	$("#ena").attr("style", "display: none");
	$("#dva").attr("style", "display: none");
	$("#tri").attr("style", "display: none");
	$("#generirajJedilnikContainer").attr("style", "");
	vstopNavadenPrijavljen = true;
	$(".navadenUporabnikPrijavljenTrue").attr("style", "");
	$("#goranSkrij").attr("style", "visibility: hidden");
	$("#topGeneriraj").attr("class", "col-lg-12 col-md-12 col-sm-12");
}



/**
 * Kreiraj nov EHR zapis za pacienta in dodaj osnovne demografske podatke.
 * V primeru uspešne akcije izpiši sporočilo s pridobljenim EHR ID, sicer
 * izpiši napako.
 */
function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}



/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preberiEHRodBolnika() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}

/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom in merilec).
 */
function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	var telesnaTemperatura = $("#dodajVitalnoTelesnaTemperatura").val();
	var merilec = $("#dodajVitalnoMerilec").val();


	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in; word-wrap: break-word'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}

var vstopNavadenPrijavljen = false;
function kreirajJedilnik() {
	sessionId = getSessionId();

	// todo: treba dobit iz ehr
	var zdajsnjaTeza = 74;
	var visina = 185;
	var starost = 27;
	var ehrId = $("#idOsebeCiljnaTeza").val();
	console.log(ehrId);



	if (vstopNavadenPrijavljen) {
		var zdajsnjaTeza = $("#telesnaTezaInput").val();
		var visina = $("#visinaInput").val();
		var starost = $("#starostInput").val();
		ehrId = "aabaa4c2-27d9-404a-8204-de69854ea5fe";
		console.log(zdajsnjaTeza + visina + starost);
	}


	var ciljnaTeza = $("#ciljnaTelesnaTezaInput").val();
	var nivoAktivnostiSelect = $("#nivoAktivnostiSelect").val();
	var ciljniCas = $("#ciljniCasInput").val();

	var glutenCB = $("#glutenCB").is(":checked");
	var mlecniCB = $("#mlecniCB").is(":checked");
	var jajcaCB = $("#jajcaCB").is(":checked");
	var sojaCB = $("#sojaCB").is(":checked");
	var psenicaCB = $("#psenicaCB").is(":checked");
	var ribeCB = $("#ribeCB").is(":checked");
	var skoljkeCB = $("#skoljkeCB").is(":checked");
	var oresckiCB = $("#oresckiCB").is(":checked");
	var arasidiCB = $("#arasidiCB").is(":checked");

	var sladkorCB = $("#sladkorCB").is(":checked");
	var alkoholCB = $("#alkoholCB").is(":checked");
	var vegiCB = $("#vegiCB").is(":checked");
	var veganskoCB = $("#veganskoCB").is(":checked");
	var paleoCB = $("#paleoCB").is(":checked");

	var alergijeList = [glutenCB, mlecniCB, jajcaCB, sojaCB, psenicaCB, ribeCB, skoljkeCB, oresckiCB, arasidiCB, sladkorCB, alkoholCB, vegiCB, veganskoCB, paleoCB];
	var alergijeDic = {
		"0": "gluten-free", 
		"1": "dairy-free", 
		"2": "egg-free", 
		"3": "soy-free", 
		"4": "wheat-free", 
		"5": "fish-free", 
		"6": "shellfish-free", 
		"7": "tree-nut-free", 
		"8": "peanut-free",
		"9": "low-sugar", 
		"10": "alcohol-free", 
		"11": "vegetarian", 
		"12": "vegan", 
		"13": "paleo", 
	
	}
	var alergijeUrl ="";
	for (var i = 0; i < alergijeList.length; i++) {
		if (alergijeList[i] == true) {
			alergijeUrl += "&health="+alergijeDic[i];
		} 
	}
	//console.log(alergijeUrl);



	var vlaknineCB = $("#vlaknineCB").is(":checked");
	var beljakovineCB = $("#beljakovineCB").is(":checked");
	var ohCB = $("#ohCB").is(":checked");
	var soliCB = $("#soliCB").is(":checked");
	var uravnotezenoCB = $("#uravnotezenoCB").is(":checked");

	var preferenceList = [vlaknineCB, beljakovineCB, ohCB, soliCB, uravnotezenoCB];
	var preferenceDic = {
		"0": "high-fiber",
		"1": "high-protein",
		"2": "low-carb",
		"3": "low-sodium",
		"4": "balanced",
	}

	var preferenceUrl = "";
	for (var i = 0; i < preferenceList.length; i++) {
		if (preferenceList[i] == true) {
			preferenceUrl += "&diet="+preferenceDic[i];
		}
	}
	//console.log(preferenceUrl);

	exerciseDic = {
		"1": 1.2, 
		"2": 1.375, 
		"3": 1.55, 
		"4": 1.725, 
		"5": 1.9, 
	}


	var kalorijeNaObrok = 0;
	if (zdajsnjaTeza > ciljnaTeza) {  //znači hujša
		kalorijeNaObrok = (((10*zdajsnjaTeza + 6.25*visina - 5*starost - 77)*exerciseDic[nivoAktivnostiSelect] - (((zdajsnjaTeza - ciljnaTeza)/ciljniCas)*1000)) - 180)/3;
	}

	else if (zdajsnjaTeza < ciljnaTeza) {  //znači pridobiva na masi
		kalorijeNaObrok = (((10*zdajsnjaTeza + 6.25*visina - 5*starost - 77)*exerciseDic[nivoAktivnostiSelect] + (((ciljnaTeza - zdajsnjaTeza)/ciljniCas)*1000)) - 180)/3;
	  
	}
	kalorijeNaObrok = Math.round(Math.abs(kalorijeNaObrok));
	console.log(kalorijeNaObrok);

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajJedilnikSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {
		    	"Accept-Encoding": 		"gzip, deflate, sdch, br",
				"Content-Type": 		"application/json",
			}
		});
		var podatki = {
			"app_id": 	"31cf15c4",
			"app_key": 	"b47850bb325f8dc3b98cd2d12889c86c",
			"q":		"",
			"calories":	"gte%20"+(kalorijeNaObrok-50)+",%20lte%20"+(kalorijeNaObrok+50),
			"from":		"0",
			"to":		"3",
		};

		$.ajax({
		    
		    url: "https://api.edamam.com/search?q="+podatki["q"]+"&app_id="+podatki["app_id"]+"&app_key="+podatki["app_key"]+"&calories="+podatki["calories"]+"&to="+podatki["to"]+"&from="+podatki["from"]+alergijeUrl+preferenceUrl,
		    type: 'GET',
		    contentType: 'application/json',
		    success: function (res) {

		    	for (var i = 0; i < 3; i++) {
		    		var urlUrl = res["hits"][i]["recipe"]["url"];

		    		var imeRecepta = res["hits"][i]["recipe"]["label"];
		    		
		    		var imgUrl = res["hits"][i]["recipe"]["image"];
		    		var yields = res["hits"][i]["recipe"]["yield"];
		    		var cals = Math.round(res["hits"][i]["recipe"]["calories"]);
		    		var stKalorijNaPorcijo = Math.round(cals/yields);



		    		switch (i) {
		    			case 0:
		    				$("#slika1").attr("src",imgUrl);
		    				$("#imeRecepta1").html(imeRecepta);
		    				$("#urlRecepta1").html("<a href='"+urlUrl+"''>"+urlUrl+"</a>");
		    				$("#stPorcij1").html(yields);
		    				$("#stKalorij1").html(cals);
		    				$("#stKalorijNaPorcijo1").html(stKalorijNaPorcijo);
		    				break;
		    			case 1:
		    				$("#slika3").attr("src",imgUrl);
		    				$("#imeRecepta3").html(imeRecepta);
		    				$("#urlRecepta3").html(urlUrl);
		    				$("#stPorcij3").html(yields);
		    				$("#stKalorij3").html(cals);
		    				$("#stKalorijNaPorcijo3").html(stKalorijNaPorcijo);
		    				break;
		    			case 2:
		    				$("#slika5").attr("src",imgUrl);
		    				$("#imeRecepta5").html(imeRecepta);
		    				$("#urlRecepta5").html(urlUrl);
		    				$("#stPorcij5").html(yields);
		    				$("#stKalorij5").html(cals);
		    				$("#stKalorijNaPorcijo5").html(stKalorijNaPorcijo);
		    				break;
		    		}
		    	}

		    	var imgBanana = "https://cdn.daysoftheyear.com/wp-content/images/banana-day1-e1429079484250-808x382.jpg";
		    	var imgApple  = "http://i2.cdn.cnn.com/cnnnext/dam/assets/150919153848-01-popular-fruits-apples-super-169.jpg";
		    	var imgPear   = "http://www.almanac.com/sites/default/files/styles/primary_image_in_article/public/images/pears.jpg";

		    	var rndSadje = Math.floor(Math.random()*3);
		    	switch (rndSadje) {
		    		case 0:
	    				$("#slika2").attr("src",imgBanana);
	    				$("#imeRecepta2").html("banana");
	    				$("#stKalorij2").html("89");

	    				$("#slika4").attr("src",imgApple);
	    				$("#imeRecepta4").html("jabolko");
	    				$("#stKalorij4").html("93");
	    				break;
	    			case 1:
	    				$("#slika2").attr("src",imgApple);
	    				$("#imeRecepta2").html("jabolko");
	    				$("#stKalorij2").html("93");

	    				$("#slika4").attr("src",imgPear);
	    				$("#imeRecepta4").html("hruška");
	    				$("#stKalorij4").html("86");
	    				break;
	    			case 2:
	    				$("#slika2").attr("src",imgPear);
	    				$("#imeRecepta2").html("hruška");
	    				$("#stKalorij2").html("86");

	    				$("#slika4").attr("src",imgBanana);
	    				$("#imeRecepta4").html("banana");
	    				$("#stKalorij4").html("89");
	    				break;
		    	}

		    	$("#opacityDiv").attr("style", "");



		        $("#dodajJedilnikSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              "success" + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajJedilnikSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            err.responseText + "'!");
		    }
		});
	}
}



/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura, filtriranje telesne temperature in telesna teža).
 * Filtriranje telesne temperature je izvedena z AQL poizvedbo, ki se uporablja
 * za napredno iskanje po zdravstvenih podatkih.
 */
function preberiMeritveVitalnihZnakov() {
	sessionId = getSessionId();


	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
	var tip = $("#preberiTipZaVitalneZnake").val();

	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
          "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>.</span><br/><br/>");
				if (tip == "telesna temperatura") {
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna temperatura</th></tr>";
						        for (var i in res) {
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].temperature +
                          " " + res[i].unit + "</td>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				} else if (tip == "telesna teža") {
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna teža</th></tr>";
                    			var seznamDatum = [];
                    			var seznamTeza = [];
						        for (var i in res) {
						        	seznamDatum.push(res[i].time);
						        	seznamTeza.push(res[i].weight);
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].weight + " " 	+
                          res[i].unit + "</td>";
						        }
						        results += "</table><canvas id='myChart' width='400' height='400'></canvas>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
						        var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: seznamDatum,
        datasets: [{
            label: 'teža[kg]',
            data: seznamTeza,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				} else if (tip == "telesna temperatura AQL") {
					var AQL =
						"select " +
    						"t/data[at0002]/events[at0003]/time/value as cas, " +
    						"t/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/magnitude as temperatura_vrednost, " +
    						"t/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/units as temperatura_enota " +
						"from EHR e[e/ehr_id/value='" + ehrId + "'] " +
						"contains OBSERVATION t[openEHR-EHR-OBSERVATION.body_temperature.v1] " +
						"where t/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/magnitude<35 " +
						"order by t/data[at0002]/events[at0003]/time/value desc " +
						"limit 10";
					$.ajax({
					    url: baseUrl + "/query?" + $.param({"aql": AQL}),
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	var results = "<table class='table table-striped table-hover'>" +
                  "<tr><th>Datum in ura</th><th class='text-right'>" +
                  "Telesna temperatura</th></tr>";
					    	if (res) {
					    		var rows = res.resultSet;
						        for (var i in rows) {
						            results += "<tr><td>" + rows[i].cas +
                          "</td><td class='text-right'>" +
                          rows[i].temperatura_vrednost + " " 	+
                          rows[i].temperatura_enota + "</td>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}

					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				}
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}


$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});

  /**
   * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
   * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
   * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
		$("#dodajVitalnoTelesnaTemperatura").val(podatki[4]);
		$("#dodajVitalnoKrvniTlakSistolicni").val(podatki[5]);
		$("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[6]);
		$("#dodajVitalnoNasicenostKrviSKisikom").val(podatki[7]);
		$("#dodajVitalnoMerilec").val(podatki[8]);
	});

  /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Ata Smrk, Pujsa Pepa)
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});

});
